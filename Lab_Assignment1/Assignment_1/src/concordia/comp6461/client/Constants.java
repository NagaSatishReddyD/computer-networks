package concordia.comp6461.client;

public class Constants {
    public static final String HELP = "help";
    public static final String GET = "get";
    public static final String POST = "post";
    public static final String HTTP = "http";
    public static final String HTTPS = "https";
    public static final int HTTP_PORT = 80;
    public static final int HTTPS_PORT = 443;
    public static final String VERBOSE = "-v";
    public static final String HEADER = "-h";
    public static final String INLINE_DATA_1 = "-d";
    public static final String INLINE_DATA_2 = "--d";
    public static final String READ_FILE = "-f";
    public static final String CREATE_FILE_CODE = "-o";
    public static final String POST_REDIRECT = "post_redirect";
    public static final String GET_REDIRECT = "get_redirect";
    public static final int HTTP_OK = 200;
    public static final int HTTP_MOVED_TEMP = 302;
    public static final int HTTP_MOVED_PERM = 301;
    public static final int HTTP_SEE_OTHER = 303;
    protected static final String HTTPC = "httpc";
}
