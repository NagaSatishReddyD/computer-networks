package concordia.comp6461.client;

import java.net.URI;
import java.net.URISyntaxException;

public class UrlData {
    URI uri;
    private String hostName;
    private String protocolName;
    private int portNumber;
    private String queryString;
    private String urlPath;

    UrlData(String url){
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    UrlData fetchUrlData(){
        this.setHostName(uri.getHost());
        this.setProtocolName(uri.getScheme());
        this.setPortNumber(uri.getPort());
        this.setQueryString(uri.getRawQuery());
        this.setUrlPath(uri.getRawPath());
        return this;
    }

    private void setUrlPath(String rawPath) {
        if(!this.queryString.isEmpty() || !this.queryString.isEmpty()){
            rawPath += "?"+this.queryString;
        }
        this.urlPath = rawPath;
    }

    private void setQueryString(String rawQuery) {
        if(rawQuery == null){
            rawQuery = "";
        }
        this.queryString = rawQuery;
    }

    private void setPortNumber(int port) {
        if(port == -1){
            if(this.protocolName.equals(Constants.HTTP)){
                port = Constants.HTTP_PORT;
            }else if(this.protocolName.equals(Constants.HTTPS)){
                port = Constants.HTTPS_PORT;
            }
        }
        this.portNumber = port;
    }

    private void setProtocolName(String scheme) {
        this.protocolName = scheme;
    }

    private void setHostName(String host) {
        if(host == null){
            host = "";
        }
        this.hostName = host;
    }

    public String getHostName() {
        return this.hostName;
    }

    public int getPortNumber() {
        return this.portNumber;
    }

    public String getUrlPath() {
        return this.urlPath;
    }
}
