package concordia.comp6461.client;

import java.io.*;
import java.io.BufferedReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * HttpClient class is the main class for the HttpClient implementation
 *
 * @author NagaSatishReddy
 * @author Namita Faujdar
 */

public class HttpClient {
    String command;
    List<String> commandParsedList;
    String commandUrl;
    UrlData urlData;
    private boolean verbose;
    private boolean header;
    private ArrayList<String> headerDataList = new ArrayList<>();
    private String inlineData;
    private boolean inLineFlag;
    private boolean readFileFlag;
    private boolean generateFileFlag;
    private String fileUpload;
    private String generateFile;
    private int statusCode;
    private String newUrl;
    private int redirectTimes = 1;

    public static void main(String[] args) {
        boolean toBeRunning = true;
        HttpClient client = new HttpClient();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while(toBeRunning) {
                String input = reader.readLine().trim();
                input = input.replace("'","");
                if (input.equalsIgnoreCase("exit")) {
                    toBeRunning = false;
                } else {
                    client.resetQueryParameters();
                    client.setCommand(input);
                    client.setCommandParseList();
                    client.parseCommand();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                reader.close();
            } catch (IOException e) {
                System.out.println("Problem in closing the buffer reader");
            }
        }
    }

    private void resetQueryParameters() {
        this.setVerbose(false);
        this.setInLineFlagData(false);
        this.setHeader(false, "");
        this.setReadFileFlag(false);
        this.setGenerateFileFlag(false);
        this.fileUpload=null;
        this.generateFile=null;
        this.newUrl=null;
        this.inlineData=null;
        this.redirectTimes = 1;
    }

    private void incrementRedirectTimes() {
        this.redirectTimes++;
    }

    private int getRedirectTimes() {
        return this.redirectTimes;
    }

    private void parseCommand() {
        if(!this.commandParsedList.get(0).equals(Constants.HTTPC)){
            printInvalidCommand();
            return;
        }
        if(this.commandParsedList.get(1).equals(Constants.HELP)){
            parseHelp();
        }else if(this.commandParsedList.get(1).equals(Constants.GET) || this.commandParsedList.get(1).equals(Constants.POST)){
            int i = 2;
            while(i < this.commandParsedList.size()){
                String currentWord = this.commandParsedList.get(i);
                if(currentWord.startsWith(Constants.HTTP) || currentWord.startsWith(Constants.HTTPS)){
                    setCommandUrl(currentWord);
                }else{
                    switch(currentWord){
                        case Constants.VERBOSE:
                            this.setVerbose(true);
                            break;
                        case Constants.HEADER:
                            i++;
                            this.setHeader(true, this.commandParsedList.get(i));
                            break;
                        case Constants.INLINE_DATA_1:
                        case Constants.INLINE_DATA_2:
                            this.setInLineFlagData(true);
                            this.inlineData = this.commandParsedList.get(++i);
                            break;
                        case Constants.READ_FILE:
                            this.setReadFileFlag(true);
                            this.fileUpload = this.commandParsedList.get(++i);
                            break;
                        case Constants.CREATE_FILE_CODE:
                            this.setGenerateFileFlag(true);
                            this.generateFile = this.commandParsedList.get(++i);
                    }
                }
                i++;
            }
            executeCommand(this.commandParsedList.get(1));
        }else{
            printInvalidCommand();
        }
    }

    private void setGenerateFileFlag(boolean b) {
        this.generateFileFlag = b;
    }

    private void setReadFileFlag(boolean b) {
        this.readFileFlag = b;
    }

    private void setInLineFlagData(boolean b) {
        this.inLineFlag = b;
    }

    private void setHeader(boolean b, String s) {
        this.header=b;
        if(b){
            if(this.headerDataList == null){
                this.headerDataList = new ArrayList<>();
            }
            this.headerDataList.add(s);
        }else{
            this.headerDataList = new ArrayList<>();
        }
    }

    private void setVerbose(boolean b) {
        this.verbose = b;
    }

    private void executeCommand(String requestMethod) {
        if(this.commandUrl == null){
            printInvalidCommand();
        }else{
            this.urlData = new UrlData(this.commandUrl).fetchUrlData();
            if(!(this.readFileFlag && this.inLineFlag)){
                    if(requestMethod.equals(Constants.GET)){
                        executeGetRequest();
                    }else if(requestMethod.equals(Constants.POST)){
                        executePostRequest();
                    }
            }else{
                System.out.println("Invalid Command: -f and -d cannot come together");
            }
        }
    }

    private void executePostRequest() {
        try (Socket socket = new Socket(this.urlData.getHostName(), this.urlData.getPortNumber())) {
            StringBuilder stringBuilder = new StringBuilder();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            writer.write(generateOutputBasedOnMethodType("POST"));
            writer.write("Host:" + this.urlData.getHostName() + "\r\n");
            if (this.header && !headerDataList.isEmpty()) {
                for (String headerData : headerDataList) {
                    String[] headerKeyValue = headerData.split(":");
                    writer.write(headerKeyValue[0] + ":" + headerKeyValue[1] + "\r\n");
                }
            }

            if (this.inLineFlag) {
                stringBuilder.append(this.inlineData);
            } else if (this.readFileFlag) {
                try{
                    BufferedReader reader = new BufferedReader(new FileReader(this.fileUpload));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    reader.close();
                }catch(FileNotFoundException exception){
                    System.out.println(exception.getMessage());
                }

            }
            writer.write("Content-Length:" + stringBuilder.toString().trim().length() + "\r\n");
            writer.write("\r\n");
            if (this.inlineData != null) {
                writer.write(this.inlineData.replace("'", "") + "\r\n");
            }
            if (stringBuilder.toString().trim().length() >= 1) {
                writer.write(stringBuilder.toString() + "\r\n");
            }
            writer.flush();
            printOutput(socket);
            writer.close();
            redirectUrl(Constants.POST_REDIRECT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeGetRequest() {
        try (Socket socket = new Socket(this.urlData.getHostName(), this.urlData.getPortNumber());
             PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {
            writer.println(generateOutputBasedOnMethodType("GET"));
            writer.println("Host:" + this.urlData.getHostName());
            for (String header : this.headerDataList) {
                String[] headerKeyValue = header.split(":");
                writer.println(headerKeyValue[0] + ":" + headerKeyValue[1]);
            }
            writer.println("\r\n");
            printOutput(socket);
            writer.close();
            redirectUrl(Constants.GET_REDIRECT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void redirectUrl(String redirectRequest) {
        if(this.getRedirectTimes() > 5){
            System.out.println("Tried 5 times But couldn't redirect");
        }else if (this.statusCode != Constants.HTTP_OK && (this.statusCode == Constants.HTTP_MOVED_TEMP
                || this.statusCode == Constants.HTTP_MOVED_PERM || this.statusCode == Constants.HTTP_SEE_OTHER)) {
                System.out.println("\nStatus Code :" + this.statusCode);
                System.out.print("Redirecting to:" + this.newUrl + "\n Please Wait............");
                this.incrementRedirectTimes();
                this.setCommandUrl(this.newUrl);
                this.urlData = new UrlData(this.commandUrl).fetchUrlData();
                if (redirectRequest.equals(Constants.GET_REDIRECT))
                    executeGetRequest();
                else if (redirectRequest.equals(Constants.POST_REDIRECT))
                    executePostRequest();
                System.out.println("Validated : Redirection");
        }
    }

    private void printOutput(Socket socket) {
        try (InputStream inputStream = socket.getInputStream();
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            boolean isFirstTime = true;
            StringBuilder receiveContent = new StringBuilder();
            do {
                line = bufferedReader.readLine();
                if(line == null){
                    break;
                }
                if (line.trim().isEmpty() && isFirstTime) {
                    receiveContent.append("\r");
                    isFirstTime = false;
                }
                receiveContent.append(line);
                receiveContent.append("\n");
            } while (!(line.endsWith("}") || line.toLowerCase().endsWith("</html>")));
            String[] receivedContent = receiveContent.toString().split("\r");
            String[] responseHeader = receivedContent[0].split("\n");
            String[] responseBody = receivedContent[1].split("\n");
            this.setStatusCode(Integer.parseInt(responseHeader[0].substring(9, 12)));
            for (String header : responseHeader) {
                if (header.startsWith("Location:")) {
                    this.setNewURL(header.substring(10));
                }
            }
            printVerbose(responseHeader);
            printTheResponseBody(responseBody);
            printToFile(responseHeader, responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setNewURL(String url) {
        this.newUrl = url;
    }

    private void setStatusCode(int code) {
        this.statusCode = code;
    }

    private void printToFile(String[] responseHeader, String[] responseBody) {
        if(this.generateFileFlag){
            if (this.generateFile != null) {
                try( PrintWriter writer = new PrintWriter(generateFile, StandardCharsets.UTF_8)){
                    writer.println("Command: " + this.command + "\r\n");
                    if (this.verbose)
                        writeOutputToFile(writer, responseHeader);

                    writer.println("");
                    writeOutputToFile(writer, responseBody);
                } catch (FileNotFoundException | UnsupportedEncodingException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeOutputToFile(PrintWriter writer, String[] response) {
        for(String message: response){
            writer.println(message);
        }
    }

    private void printVerbose(String[] responseHeader) {
        if(this.verbose){
            printTheResponseBody(responseHeader);
        }
    }

    private void printTheResponseBody(String[] responseBody) {
        for(String data: responseBody){
            System.out.println(data);
        }
    }

    private String generateOutputBasedOnMethodType(String requestMethod) {
        boolean urlPathIsEmpty = this.urlData.getUrlPath().isEmpty();
        StringBuilder stringBuilder = new StringBuilder();
        String endHttp = requestMethod.equals("POST") ? "HTTP/1.1\r\n": urlPathIsEmpty ? "/1.1" : "HTTP/1.1";
        stringBuilder.append(requestMethod).append(" ").append(urlPathIsEmpty ? "/": this.urlData.getUrlPath()).append(" ").append(endHttp);
        return stringBuilder.toString();
    }

    private void setCommandUrl(String currentWord) {
        this.commandUrl = currentWord;
    }

    private void printInvalidCommand() {
        System.out.println("Invalid Command");
    }

    private void parseHelp() {
        if(this.commandParsedList.size() >3){
            printInvalidCommand();
            return;
        }
        String lastWord = this.commandParsedList.get(this.commandParsedList.size()-1);
        if(lastWord.equals(Constants.GET)){
            System.out.println("httpc help get\n" +
                    "usage: httpc get [-v] [-h key:value] URL\n" +
                    "Get executes a HTTP GET request for a given URL.\n" +
                    " -v Prints the detail of the response such as protocol, status,\n" +
                    "and headers.\n" +
                    " -h key:value Associates headers to HTTP Request with the format\n" +
                    "'key:value'.");
        }else if(lastWord.equals(Constants.POST)){
            System.out.println("httpc help post\n" +
                    "Comp 6461 – Fall 2020 - Lab Assignment # 1 Page 7\n" +
                    "usage: httpc post [-v] [-h key:value] [-d inline-data] [-f file] URL\n" +
                    "Post executes a HTTP POST request for a given URL with inline data or from\n" +
                    "file.\n" +
                    " -v Prints the detail of the response such as protocol, status,\n" +
                    "and headers.\n" +
                    " -h key:value Associates headers to HTTP Request with the format\n" +
                    "'key:value'.\n" +
                    " -d string Associates an inline data to the body HTTP POST request.\n" +
                    " -f file Associates the content of a file to the body HTTP POST\n" +
                    "request.\n" +
                    "Either [-d] or [-f] can be used but not both.");
        }else{
            System.out.println("httpc help\n" +
                    "httpc is a curl-like application but supports HTTP protocol only.\n" +
                    "Usage:\n" +
                    " httpc command [arguments]\n" +
                    "The commands are:\n" +
                    " get executes a HTTP GET request and prints the response.\n" +
                    " post executes a HTTP POST request and prints the response.\n" +
                    " help prints this screen.\n" +
                    "Use \"httpc help [command]\" for more information about a command.");
        }
    }

    private void setCommandParseList() {
        this.commandParsedList = Stream.of(this.command.trim().split(" ")).filter(item->item.length() > 0).map(String::trim).collect(Collectors.toList());
    }

    private void setCommand(String input) {
        this.command = input;
    }
}
