import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketAddress;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.channels.SelectionKey.OP_READ;

public class Client {

    private static final List<Long> receivedPackets = new ArrayList<>();
    private static int sequenceNum;
    private static int ackCount;
    private static final long timeout = 3000;

    public static void main(String[] args) throws IOException {

        boolean isMultiThread = false;
        List<String> commands = new ArrayList<String>();
        commands.add("httpc http://localhost:8080/get/");
        commands.add("httpc http://localhost:8080/post/test.txt -d '{\"Assignment\":123qqqasq9090}'");
        clientRequestCreation(isMultiThread, commands);
    }

    private static void clientRequestCreation(boolean isMultiThread, List<String> commands) throws IOException {
        // Router address
        String routerHost = "localhost";
        int routerPort = 3000;

        File file = new File("attachment");
        file.mkdir();
        while (true) {
            String url = "";
            receivedPackets.clear();
            sequenceNum = 0;
            ackCount = 0;
            if(!isMultiThread) {

                Scanner sc = new Scanner(System.in);
                commands.add(sc.nextLine());
            }

            for(String request: commands){
                if (request.isEmpty()) {
                    System.out.println("Invalid Command");
                    continue;
                }
                List<String> requestList = Stream.of(request.split(" ")).filter(data -> data.trim().startsWith("http://")).collect(Collectors.toList());

                for (String s : requestList) {
                    if (s.startsWith("http://"))
                        url = s;
                }
                Client c = new Client();
                if(isMultiThread) {
                    String finalUrl = url;
                    Runnable task = () -> {
                        String serverHost = null;
                        try {
                            serverHost = new URL(finalUrl).getHost();
                            int serverPort = new URL(finalUrl).getPort();
                            SocketAddress routerAddress = new InetSocketAddress(routerHost, routerPort);
                            InetSocketAddress serverAddress = new InetSocketAddress(serverHost, serverPort);

                            c.startConnection(routerAddress, serverAddress);
                            c.runClient(routerAddress, serverAddress, request);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    };
                    new Thread(task).start();
                }else{
                    String serverHost = new URL(url).getHost();
                    int serverPort = new URL(url).getPort();

                    SocketAddress routerAddress = new InetSocketAddress(routerHost, routerPort);
                    InetSocketAddress serverAddress = new InetSocketAddress(serverHost, serverPort);

                    startConnection(routerAddress, serverAddress);
                    runClient(routerAddress, serverAddress, request);
                }

            }
            if(isMultiThread){
                break;
            }

        }
    }

    private static void runClient(SocketAddress routerAddress, InetSocketAddress serverAddress, String request) throws IOException {

        String dir = System.getProperty("user.dir");
        DatagramChannel channel = DatagramChannel.open();
        sequenceNum++;
        Packet p = new Packet.Builder().setType(0).setSequenceNumber(sequenceNum)
                .setPortNumber(serverAddress.getPort()).setPeerAddress(serverAddress.getAddress())
                .setPayload(request.getBytes()).build();
        channel.send(p.toBuffer(), routerAddress);
        System.out.println("sending request to Router...>");

        // Try to receive a packet within timeout.
        channel.configureBlocking(false);
        Selector selector = Selector.open();
        channel.register(selector, OP_READ);
        selector.select(timeout);

        Set<SelectionKey> keys = selector.selectedKeys();
        if (keys.isEmpty()) {
            System.out.println("No response after timeout\nSending again");
            resend(channel, p, routerAddress);
        }

        // We just want a single response.
        ByteBuffer byteBuffer = ByteBuffer.allocate(Packet.MAX_LEN);
        SocketAddress router = channel.receive(byteBuffer);
        byteBuffer.flip();
        Packet resp = Packet.fromBuffer(byteBuffer);
        String payload = new String(resp.getPayload(), StandardCharsets.UTF_8);

        if (!receivedPackets.contains(resp.getSequenceNumber())) {
            receivedPackets.add(resp.getSequenceNumber());

            if (request.contains("Content-Disposition:attachment")) {
                String[] responseArray = payload.split("\\|");

                File file = new File(dir + "/attachment/" + responseArray[1].trim());

                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);

                pw.print(responseArray[2]);
                pw.flush();
                pw.close();

                System.out.println(responseArray[0]);
                System.out.println("File downloaded in " + dir + "\\attachment");
            } else {
                System.out.println(payload);
            }

            // Sending ACK for the received of the response
            sequenceNum++;
            Packet pAck = new Packet.Builder().setType(0).setSequenceNumber(sequenceNum)
                    .setPortNumber(serverAddress.getPort()).setPeerAddress(serverAddress.getAddress())
                    .setPayload("Received".getBytes()).build();
            channel.send(pAck.toBuffer(), routerAddress);

            // Try to receive a packet within timeout.
            channel.configureBlocking(false);
            selector = Selector.open();
            channel.register(selector, OP_READ);
            selector.select(timeout);

            keys = selector.selectedKeys();
            if (keys.isEmpty()) {
                resend(channel, pAck, router);
            }

            byteBuffer.flip();

            System.out.println("Connection closed..!");
            keys.clear();

            sequenceNum++;
            Packet pClose = new Packet.Builder().setType(0).setSequenceNumber(sequenceNum)
                    .setPortNumber(serverAddress.getPort()).setPeerAddress(serverAddress.getAddress())
                    .setPayload("Ok".getBytes()).build();
            channel.send(pClose.toBuffer(), routerAddress);
            System.out.println("OK sent");
        }
    }


    private static void resend(DatagramChannel channel, Packet p, SocketAddress routerAddress) throws IOException {

        channel.send(p.toBuffer(), routerAddress);
        System.out.println(new String(p.getPayload()));
        if (new String(p.getPayload()).equals("Received")) {
            ackCount++;
        }

        channel.configureBlocking(false);
        Selector selector = Selector.open();
        channel.register(selector, OP_READ);
        selector.select(timeout);

        Set<SelectionKey> keys = selector.selectedKeys();
        if (keys.isEmpty() && ackCount < 10) {

            System.out.println("No response after timeout\nSending again");
            resend(channel, p, routerAddress);

        }

    }

    private static void startConnection(SocketAddress routerAddress, InetSocketAddress serverAddress) throws IOException {
        DatagramChannel channel = DatagramChannel.open();
        String msg = "Hi from Client";
        sequenceNum++;
        // SYN
        Packet p = new Packet.Builder().setType(0).setSequenceNumber(sequenceNum)
                .setPortNumber(serverAddress.getPort()).setPeerAddress(serverAddress.getAddress())
                .setPayload(msg.getBytes()).build();
        channel.send(p.toBuffer(), routerAddress);
        System.out.println("Sending Hi from Client");

        channel.configureBlocking(false);
        Selector selector = Selector.open();
        channel.register(selector, OP_READ);

        selector.select(timeout);

        Set<SelectionKey> keys = selector.selectedKeys();
        if (keys.isEmpty()) {
            System.out.println("No response after timeout\nSending again");
            resend(channel, p, routerAddress);
        }

        ByteBuffer buf = ByteBuffer.allocate(Packet.MAX_LEN);
        //buf.flip();
        Packet resp = Packet.fromBuffer(buf);
        String payload = new String(resp.getPayload(), StandardCharsets.UTF_8);
        System.out.println(payload + " received..!");
        receivedPackets.add(resp.getSequenceNumber());
        keys.clear();
    }
}
