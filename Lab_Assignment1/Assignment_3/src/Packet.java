import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Packet {
    public static final int MAX_LEN = 11+1024;
    private static int MIN_LEN = 11;
    private final int type;
    private final long sequenceNumber;
    private final InetAddress peerAddress;
    private final int peerPort;
    private final byte[] payload;

    public Packet(int type, long sequenceNumber, InetAddress peerAddress, int portNumber, byte[] payload) {
        this.type = type;
        this.sequenceNumber = sequenceNumber;
        this.peerAddress = peerAddress;
        this.peerPort = portNumber;
        this.payload = payload;
    }


    public static Packet fromBuffer(ByteBuffer byteBuffer) throws IOException {
        if(byteBuffer.limit() < MIN_LEN || byteBuffer.limit() > MAX_LEN){
            throw new IOException("Invalid length");
        }

        Builder builder = new Builder();

        builder.setType(Byte.toUnsignedInt(byteBuffer.get()));
        builder.setSequenceNumber(Integer.toUnsignedLong(byteBuffer.getInt()));

        byte[] host = new byte[] { byteBuffer.get(), byteBuffer.get(), byteBuffer.get(), byteBuffer.get() };
        builder.setPeerAddress(Inet4Address.getByAddress(host));
        builder.setPortNumber(Short.toUnsignedInt(byteBuffer.getShort()));

        byte[] payload = new byte[byteBuffer.remaining()];
        byteBuffer.get(payload);
        builder.setPayload(payload);

        return builder.build();
    }

    public byte[] getPayload() {
        return this.payload;
    }

    public Builder toBuilder() {
        return new Builder().setType(type).setSequenceNumber(sequenceNumber).setPeerAddress(peerAddress)
                .setPortNumber(peerPort).setPayload(payload);
    }

    public ByteBuffer toBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(MAX_LEN).order(ByteOrder.BIG_ENDIAN);
        write(byteBuffer);
        byteBuffer.flip();
        return byteBuffer;
    }

    private void write(ByteBuffer byteBuffer) {
        byteBuffer.put((byte) type);
        byteBuffer.putInt((int) sequenceNumber);
        byteBuffer.put(peerAddress.getAddress());
        byteBuffer.putShort((short) peerPort);
        byteBuffer.put(payload);
    }

    public Long getSequenceNumber() {
        return this.sequenceNumber;
    }

    public static class Builder{
        private int type;
        private long sequenceNumber;
        private InetAddress peerAddress;
        private int portNumber;
        private byte[] payload;

        public Builder setType(int type) {
            this.type = type;
            return this;
        }

        public Builder setSequenceNumber(long sequenceNumber) {
            this.sequenceNumber = sequenceNumber;
            return this;
        }

        public Builder setPeerAddress(InetAddress peerAddress) {
            this.peerAddress = peerAddress;
            return this;
        }

        public Builder setPortNumber(int portNumber) {
            this.portNumber = portNumber;
            return this;
        }

        public Builder setPayload(byte[] payload) {
            this.payload = payload;
            return this;
        }

        public Packet build() {
            return new Packet(type, sequenceNumber, peerAddress, portNumber, payload);
        }
    }
}
