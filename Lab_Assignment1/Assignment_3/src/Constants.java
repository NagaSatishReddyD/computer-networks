import jdk.nashorn.internal.ir.debug.ClassHistogramElement;

public class Constants {
    public static final String VERBOSE = "-v";
    public static final String PORT = "-p";
    public static final String DIRECTORY = "-d";
    public static final String HI_FROM_CLIENT = "Hi from Client";
    public static final String HI_FROM_SERVER = "Hi from Server";
    public static final String SEND_HI_FROM_SERVER = "Sending Hi From Server";
    public static final String HTTPFS = "httpfs";
    public static final String HTTPC = "httpc";
    public static final String OK_STATUS_CODE = "HTTP/1.1 200 OK";
    public static final String FILE_NOT_FOUND_STATUS_CODE = "HTTP/1.1 404 FILE NOT FOUND";
    public static final String FILE_OVERWRITTEN_STATUS_CODE = "HTTP/1.1 201 FILE OVER-WRITTEN";
    public static final String NEW_FILE_CREATED_STATUS_CODE = "HTTP/1.1 202 NEW FILE CREATED";
    public static final String CONNECTIONA_LIVE = "Connection: keep-alive";
    public static final String SERVER = "Server: httpfs/1.0.0";
    public static final String HEADER = "-h";
    public static final String CREATE_NEW_FILE = "-o";
    static final String DATE = "Date: ";
    static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin: *";
    static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials: true";
    static final String VIA = "Via : 1.1 vegur";
}
