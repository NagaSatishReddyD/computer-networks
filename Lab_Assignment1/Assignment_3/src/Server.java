import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Server {
    private static boolean debug;
    private static File currentDirectory;
    private static String directory;

    public static void main(String[] args) {
        String request;
        Scanner scan = new Scanner(System.in);
        request = scan.nextLine();
        if(request.isEmpty())
            System.out.println("Invalid command .. !");
        List<String> serverRequestList = Stream.of(request.split(" ")).map(String::trim).collect(Collectors.toList());
        if(serverRequestList.contains(Constants.VERBOSE)){
            debug = true;
        }
        int port;
        if(serverRequestList.contains(Constants.PORT)){
            port = Integer.parseInt(serverRequestList.get(serverRequestList.indexOf(Constants.PORT)+1));
        }else{
             port = 8080;
        }
        if(serverRequestList.contains(Constants.DIRECTORY)){
            directory = serverRequestList.get(serverRequestList.indexOf(Constants.DIRECTORY)+1);
        }else{
            directory = System.getProperty("user.dir");
        }
        currentDirectory = new File(directory);

        Server server = new Server();
        Runnable task = ()->{
            try {
                server.startServer(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        new Thread(task).start();
    }

    private void startServer(int port) throws IOException {
        System.out.format("Server Started at port : %s...",port);
        DatagramChannel channel = DatagramChannel.open();
        channel.bind(new InetSocketAddress(port));
        ByteBuffer byteBuffer = ByteBuffer.allocate(Packet.MAX_LEN).order(ByteOrder.BIG_ENDIAN);
        while(true){
            byteBuffer.clear();
            SocketAddress router = channel.receive(byteBuffer);
            if(router != null){
                byteBuffer.flip();
                Packet packet = Packet.fromBuffer(byteBuffer);
                byteBuffer.flip();

                String requestPayload = new String(packet.getPayload(), UTF_8);

                if(requestPayload.equals(Constants.HI_FROM_CLIENT)){
                    System.out.println(requestPayload);
                    Packet resp = packet.toBuilder().setPayload(Constants.HI_FROM_SERVER.getBytes()).build();
                    channel.send(resp.toBuffer(), router);
                    System.out.println(Constants.SEND_HI_FROM_SERVER);
                }else if(requestPayload.contains(Constants.HTTPFS) || requestPayload.contains(Constants.HTTPC)){
                    String responsePayload = processPayload(requestPayload);
                    Packet resp = packet.toBuilder().setPayload(responsePayload.getBytes()).build();
                    channel.send(resp.toBuffer(), router);
                }else if(requestPayload.equals("Received")){
                    System.out.println(requestPayload);
                    Packet resp = packet.toBuilder().setPayload("Close".getBytes()).build();
                    channel.send(resp.toBuffer(), router);
                }else if(requestPayload.equals("Ok")){
                    System.out.println(requestPayload+"received..");
                }
            }
        }
    }

    private String processPayload(String requestPayload) throws IOException {

        String method ="";
        String url="";

        String[] clientRequestArray = requestPayload.split(" ");
        ArrayList<String> clientRequestList = new ArrayList<>();
        boolean isHeaderRequest=false;
        String newFileName = "";
        List<String> headerList = new ArrayList<>();
        for (int i = 0; i < clientRequestArray.length; i++) {
            clientRequestList.add(clientRequestArray[i]);
            if(clientRequestArray[i].equals(Constants.HEADER)){
                isHeaderRequest = true;
            }else if(isHeaderRequest){
                headerList.add(clientRequestArray[i]);
                isHeaderRequest = false;
            }else if(clientRequestArray[i].equals(Constants.CREATE_NEW_FILE)){
                newFileName = clientRequestArray[i + 1];
            }else if (clientRequestArray[i].startsWith("http://")) {
                url = clientRequestArray[i];
                String[] methodarray = clientRequestArray[i].split("/");
                if (methodarray.length == 4) {
                    method = methodarray[3] + "/";
                } else if (methodarray.length == 5) {

                    method = methodarray[3] + "/" + methodarray[4];

                }
            }

        }
        StringBuilder fileData = new StringBuilder();
        String downloadFileName = "";

        String host = new URL(url).getHost();
        String responseHeaders = getResponseHeaders(Constants.OK_STATUS_CODE);

        if (debug)
            System.out.println(" Server is Processing the httpfs request");
        StringBuilder body = new StringBuilder();
        body.append("{\n");
        body.append("\t\"args\":");
        body.append("{},\n");
        body.append("\t\"headers\": {");

        if (!method.endsWith("/") && method.contains("get/")
                && requestPayload.contains("Content-Disposition:attachment")) {
            body.append("\n\t\t\"Content-Disposition\": \"attachment\",");
        } else if (!method.endsWith("/") && method.contains("get/")
                && requestPayload.contains("Content-Disposition:inline")) {
            body.append("\n\t\t\"Content-Disposition\": \"inline\",");
        }
        body.append("\n\t\t\"Connection\": \"close\",\n").append("\t\t\"Host\": \"").append(host).append("\"\n").append("\t},\n");
        Map<String, String> headersMap = new HashMap<>();
        for(String data: headerList){
            String[] list = data.split(":");
            headersMap.put(list[0], list[1]);
        }

        if (method.equalsIgnoreCase("get/")) {
            processGetRequest(body, requestPayload, clientRequestList);
        }else if (!method.endsWith("/") && method.contains("get/")) {

            String requestedFileName = method.split("/")[1];
            List<String> files = getFilesFromDir(currentDirectory);

            if (requestPayload.contains("Content-Type")) {
                String fileType = clientRequestList.get(clientRequestList.indexOf("-h") + 1).split(":")[1];
                requestedFileName = requestedFileName + "." + fileType;
            }

            if (!files.contains(requestedFileName)) {
                responseHeaders = getResponseHeaders(Constants.FILE_NOT_FOUND_STATUS_CODE);
            } else {
                File file = new File(directory + "/" + requestedFileName);
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String st;
                while ((st = reader.readLine()) != null) {
                    fileData.append(st);
                }
                if (requestPayload.contains("Content-Disposition:attachment")) {
                    downloadFileName = newFileName.isEmpty()?requestedFileName: newFileName;
                } else {
                    body.append("\t\"data\": \"").append(fileData).append("\",\n");
                }

            }
        }

        else if (!method.endsWith("/") && method.contains("post/")) {

            String fileName = method.split("/")[1];
            File file = new File(fileName);
            List<String> files = getFilesFromDir(currentDirectory);
            String substring = requestPayload.substring(requestPayload.indexOf("-d") + 3);
            if (files.contains(fileName)) {
                synchronized (file) {
                    boolean isOverWrite = !hasOverwriteHeader(headersMap);
                    file = new File(directory + "/" + fileName);
                    FileWriter fw = new FileWriter(file, isOverWrite);
                    fw.write(substring);
                    fw.close();
                }
                responseHeaders = getResponseHeaders(Constants.FILE_OVERWRITTEN_STATUS_CODE);
            }

            else {
                file = new File(directory + "/" + fileName);
                synchronized (file) {
                    FileWriter fw = new FileWriter(file);
                    BufferedWriter bw = new BufferedWriter(fw);
                    PrintWriter pw = new PrintWriter(bw);

                    pw.print(substring);
                    pw.flush();
                    pw.close();
                }
                responseHeaders = getResponseHeaders(Constants.NEW_FILE_CREATED_STATUS_CODE);
            }
        }
        body.append("\t\"origin\": \"").append(InetAddress.getLocalHost().getHostAddress()).append("\",\n").append("\t\"url\": \"").append(url).append("\"\n").append( "}");

        if (debug)
            System.out.println("Sending Response to Client..");
        String responsePayload = responseHeaders + body;
        if (requestPayload.contains("Content-Disposition:attachment")) {
            responsePayload = responsePayload + "|" + downloadFileName + "|" + fileData;
        }
        return responsePayload;

    }

    private void processGetRequest(StringBuilder body, String requestPayload, ArrayList<String> clientRequestList) {

            body.append("\t\"files\": { ");
            List<String> files = getFilesFromDir(currentDirectory);
            List<String> fileFilterList = new ArrayList<>(files);

            if (requestPayload.contains("Content-Type")) {

                String fileType = clientRequestList.get(clientRequestList.indexOf("-h") + 1).split(":")[1];
                fileFilterList = files.stream().filter(file->file.endsWith(fileType)).collect(Collectors.toList());
            }

            if (!fileFilterList.isEmpty()) {
                for (int i = 0; i < fileFilterList.size(); i++) {

                    if (i != fileFilterList.size() - 1) {
                        body.append(fileFilterList.get(i)).append(" , ");
                    } else {
                        body.append(fileFilterList.get(i)).append(" },\n");
                    }

                }
            } else {
                body.append( " },\n");
            }

    }

    private boolean hasOverwriteHeader(Map<String, String> headersMap) {
        if(headersMap.containsKey("overwrite")){
           return Boolean.parseBoolean(headersMap.get("overwrite"));
        }
        return false;
    }

    private List<String> getFilesFromDir(File currentDirectory) {
        return Stream.of(Objects.requireNonNull(currentDirectory.listFiles())).filter(file->!file.isDirectory()).map(File::getName).collect(Collectors.toList());
    }

    private static String getResponseHeaders(String status){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String datetime = dateFormat.format(date);
        return status + "\n" + Constants.CONNECTIONA_LIVE + "\n" + Constants.SERVER + "\n" + Constants.DATE + datetime + "\n" + Constants.ACCESS_CONTROL_ALLOW_ORIGIN + "\n" + Constants.ACCESS_CONTROL_ALLOW_CREDENTIALS + "\n" + Constants.VIA + "\n";
    }
}
