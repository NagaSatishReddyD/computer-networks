import jdk.nashorn.internal.objects.Global;

import java.io.*;
import java.net.Socket;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HttpServerThread implements Runnable {
    private final Socket clientSocket;
    private final String directoryPath;
    private final int clientCounter;
    private final ArrayList<String> fileHeader;
    private final ArrayList<String> filesList;
    private final HashMap<String, String> parameters;
    private final HashMap<String, String> headers;
    private boolean httpcFlag = false;
    private String curlRequest="";
    private boolean httpfsFlag = false;
    private String clientRequest = "";
    private boolean contentTypeFlag = false;
    private boolean dispositionFlag = false;
    private String body;
    private String content;
    private String statusCode;
    private String uri;
    private boolean attachmentFlag = false;
    private boolean fileFlag = false;
    private boolean inLineFlag = false;

    public HttpServerThread(Socket clientSocket, String directoryPath, int clientCounter) {
        this.clientSocket = clientSocket;
        this.directoryPath = directoryPath;
        this.clientCounter = clientCounter;
        Instant start = Instant.now();
        this.fileHeader = new ArrayList<String>();
        this.filesList = new ArrayList<String>();
        this.parameters = new HashMap<String, String>();
        this.headers = new HashMap<String, String>();
        this.headers.put("Connection", "keep-alive");
        this.headers.put("Host","localhost");
        this.headers.put("Date", start.toString());
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            PrintWriter writer = new PrintWriter(this.clientSocket.getOutputStream());
            int count = 0;
            String inputRequest;
            while((inputRequest = reader.readLine()) != null){
                if(inputRequest.endsWith(Constants.HTTP_VERSION)) {
                    this.httpcFlag = true;
                    this.curlRequest = inputRequest;
                }else if(inputRequest.matches("(GET|POST)/(.*)")){
                    this.httpfsFlag = true;
                    this.clientRequest = inputRequest;
                }
                if (this.httpfsFlag) {
                    if (inputRequest.isEmpty())
                        break;
                    fileHeader.add(inputRequest);
                    this.contentTypeFlag = inputRequest.startsWith(Constants.CONTENT_TYPE) ? true : this.contentTypeFlag;
                    this.dispositionFlag = inputRequest.startsWith(Constants.CONTENT_DISPOSITION) ? true
                            : this.dispositionFlag;
                    System.out.println("Input Request " + inputRequest);
                    System.out.println("CONTENT_TYPE " +inputRequest.startsWith(Constants.CONTENT_TYPE));

                    System.out.println("disposition " +inputRequest.startsWith(Constants.CONTENT_DISPOSITION));

                    this.body = inputRequest.startsWith(Constants.INLINE_DATA_CODE2) ? inputRequest.substring(2): this.body;
                }
                if (this.httpcFlag) {
                    if (inputRequest.matches(Constants.REG1)) {
                        if (count == 0) {
                            addToHeaders(inputRequest);
                        }
                    }
                    if (count == 1) {
                        this.content = inputRequest;
                        break;
                    }
                    if (inputRequest.isEmpty())
                        count++;
                }

            }

            processClientData(writer);
            writer.println("");
            writer.flush();
            reader.close();
            this.clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processClientData(PrintWriter writer) {
        if (this.httpcFlag && this.curlRequest.matches("(GET|POST) /(.*)")) {
            processCurlRequest(writer);
        }
        if (this.httpfsFlag) {
            System.out.println("Client Request : " + this.clientRequest);
            if (this.clientRequest.startsWith(Constants.GET_METHOD)) {
                processGetRequest(writer);
            } else if (this.clientRequest.startsWith(Constants.POST_METHOD)) {
                processPostRequest(writer);
            }
        }
    }

    private synchronized void processPostRequest(PrintWriter writer) {
        if (!this.clientRequest.substring(5).contains(Constants.SLASH)) {
            try {
                File filePath = this.contentTypeFlag
                        ? new File(this.directoryPath + Constants.SLASH + this.clientRequest.substring(5)
                        + getFileContentHeader())
                        : new File(this.directoryPath + Constants.SLASH + this.clientRequest.substring(5));
                PrintWriter printWriter;
                if(hasOverwriteHeaderWithFalse()){
                     printWriter = new PrintWriter(new FileOutputStream(filePath, true));
                }else{
                    printWriter = new PrintWriter(new FileOutputStream(filePath, false));
                }
//                if(fileHeader.contains("overwrite")){
//                    boolean value = Boolean.parseBoolean(fileHeader.get("overwrite"));
//                }
                //System.out.println("---> 0" +getClientRequest());
                printWriter.println(this.body);
                writer.println("Operation Status : Succcess");
                printWriter.close();
            } catch (FileNotFoundException e) {
                writer.print(Constants.HTTP_404_ERROR);
            }
        } else {
            printOutput("Error: " + Constants.ACCESS_DENIED, writer);
        }

    }

    private boolean hasOverwriteHeaderWithFalse() {
        String booleanValue = "true";
        for(String value: fileHeader){
            if(value.startsWith("overwrite")){
                booleanValue = value.split(":")[1];
                break;
            }
        }
        return booleanValue.equals("false");
    }

    private void printOutput(String msg, PrintWriter writer) {
        writer.println(msg);
        System.out.println(msg);
    }

    private String getFileContentHeader() {
        String ext = "";
        for (int i = 0; i < fileHeader.size(); i++) {
            if (fileHeader.get(i).trim().startsWith(Constants.CONTENT_TYPE)) {
                String[] temp = fileHeader.get(i).trim().split(":");
                ext = getExtension(temp[1].trim());
            }
        }
        return ext.trim();
    }

    private String getExtension(String extension) {
        String temp = "";
        switch (extension.trim()) {
            case "application/text":
                temp = ".txt";
                break;
            case "application/json":
                temp = ".json";
                break;
            case "application/xml":
                temp = ".xml";
                break;
            default:
                temp = "";
                break;
        }
        return temp;
    }

    private synchronized void processGetRequest(PrintWriter writer) {

        String fileName = this.contentTypeFlag ? this.clientRequest.substring(4) + getFileContentHeader()
                : this.clientRequest.substring(4);
        File filePath = new File(retrieveFilePath(fileName));
        if(retrieveFilePath(fileName).contains("/..")) {
            printOutput("Error: " + Constants.ACCESS_DENIED, writer);
        } else if (this.contentTypeFlag) {
            try {
                if (filePath.exists()) {
                    BufferedReader br = new BufferedReader(new FileReader(filePath));
                    String line;
                    writer.println("File Content");
                    while ((line = br.readLine()) != null) {
                        writer.println(line);
                    }
                    writer.println("Operation Status : Success");
                    br.close();
                } else {
                    printOutput(Constants.HTTP_404_ERROR, writer);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (!fileName.contains(Constants.SLASH)) {
            if (filePath.exists()) {
                if (filePath.isDirectory()) {
                    HashMap<String, ArrayList<String>> output = new HashMap<>();
                    output.put(Constants.DIRECTORY, new ArrayList<String>());
                    output.put(Constants.FILE, new ArrayList<String>());
                    for (File file : filePath.listFiles()) {
                        if (file.isDirectory()) {
                            ArrayList<String> temp = output.get(Constants.DIRECTORY);
                            temp.add(file.getName());
                            output.replace(Constants.DIRECTORY, temp);
                        } else if (file.isFile()) {
                            ArrayList<String> temp = output.get(Constants.FILE);
                            temp.add(file.getName());
                            output.replace(Constants.FILE, temp);
                        }
                    }
                    System.out.println("------------\nDIRECTORIES: \n------------");
                    for (Map.Entry<String, ArrayList<String>> entry : output.entrySet()) {
                        ArrayList<String> temp = entry.getValue();
                        for (int i = 0; i < temp.size(); i++) {
                            if (entry.getKey().equals(Constants.DIRECTORY)) {
                                printOutput(entry.getKey().trim() + temp.get(i).trim(), writer);
                            }
                        }
                    }
                    System.out.println("------------\n FILES : \n------------");
                    for (Map.Entry<String, ArrayList<String>> entry : output.entrySet()) {
                        ArrayList<String> temp = entry.getValue();
                        for (int i = 0; i < temp.size(); i++) {
                            if (entry.getKey().equals(Constants.FILE)) {
                                printOutput(entry.getKey().trim() + temp.get(i).trim(), writer);
                            }
                        }
                    }
                } else if (filePath.isFile()) {
                    PrintWriter fileWriter = null;
                    File downloadPath = new File( Constants.PATH_TO_DOWNLOAD);
                    if (this.dispositionFlag) {
                        getFileDispositionHeader();
                        if ((this.attachmentFlag && !downloadPath.exists())) {
                            System.out.println("Download Folder Created : "
                                    + new File(this.directoryPath + Constants.PATH_TO_DOWNLOAD).mkdir());
                        }
                    }
                    try {
                        if (this.attachmentFlag) {
                            fileWriter = this.fileFlag
                                    ? new PrintWriter(downloadPath.getPath() + Constants.SLASH + getFileDispositionHeader())
                                    : new PrintWriter(downloadPath.getPath() + Constants.SLASH + fileName);
                        }
                        BufferedReader br = new BufferedReader(new FileReader(filePath));
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (this.dispositionFlag) {
                                if (this.inLineFlag) {
                                    writer.println(line);
                                } else if (this.attachmentFlag) {
                                    fileWriter.println(line);
                                }
                            } else {
                                writer.println(line);
                            }

                        }
                        writer.println("Operation Status : Success");
                        if (this.attachmentFlag)
                            fileWriter.close();
                        br.close();
                    } catch (FileNotFoundException e) {
                        printOutput(Constants.HTTP_404_ERROR + Constants.FILE_NOT_FOUND, writer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else {
                printOutput(Constants.HTTP_404_ERROR, writer);
            }
        } else {
            printOutput("Error: " + Constants.ACCESS_DENIED, writer);
        }

    }

    private String getFileDispositionHeader() {
        String name = "";
        for (int i = 0; i < fileHeader.size(); i++) {
            if (fileHeader.get(i).trim().startsWith(Constants.CONTENT_DISPOSITION)) {
                String[] temp = fileHeader.get(i).trim().split(";");
                String[] temp2 = temp[0].trim().split(":");
                if (temp2[1].trim().equals("attachment")) {
                    this.attachmentFlag = true;
                    if (temp.length == 2) {
                        String[] temp3 = temp[1].trim().split(":");
                        name = temp3[1].trim();
                        this.fileFlag = true;
                    }
                }
                if (temp2[1].trim().equals("inline")) {
                    this.inLineFlag = true;
                }
            }
        }
        return name.trim();
    }

    private String retrieveFilePath(String fileName) {
        return this.directoryPath + Constants.SLASH + fileName;
    }

    private synchronized void processCurlRequest(PrintWriter writer) {
        this.curlRequest = this.curlRequest.replace("GET /", "");
        this.curlRequest = this.curlRequest.replace("POST /", "");
        this.curlRequest = this.curlRequest.replace("HTTP/1.1", "");
        this.statusCode = Constants.HTTP_200;
        this.uri = "http://" + Constants.IP_ADDRESS + ":" + this.clientSocket.getPort() + "/" + this.curlRequest;
        writer.println("HTTP/1.0 " + this.statusCode + " " + getConnectionState() + "\r\n" + getHeaders());
        checkCurlOption(writer);
    }

    private void checkCurlOption(PrintWriter writer) {
        if (this.curlRequest.startsWith("get")) {
            System.out.println("CURL : Request Type GET");
            getCurlOption(writer);
        } else if (this.curlRequest.startsWith("post")) {
            System.out.println("CURL : Request Type POST");
            postCurlOption(writer);
        }
    }

    private void postCurlOption(PrintWriter writer) {
        this.curlRequest = this.curlRequest.replace("post?", "");
        if (!this.curlRequest.isEmpty() && this.curlRequest.matches("(.*)=(.*)")) {
            if (this.curlRequest.matches(Constants.REG2)) {
                String[] temp = this.curlRequest.split("&");
                for (int i = 0; i < temp.length; i++) {
                    setParameters(temp[i]);
                }
            } else {
                setParameters(this.curlRequest);
            }
        }
        printOutput(postBody(), writer);
    }

    private String postBody() {
        return
                "{\r\n"+" "+
                        "\"args\":{"+" "+
                        getParameters()+"},\r\n"+" "+
                        "\"data\":{"+" "+
                        this.content+"},\r\n"+" "+
                        "\"files\":{\r\n"+" "+
                        getFiles()+"},\r\n"+" "+
                        "\"headers\":{\r\n"+
                        getHeaders()+" },\r\n"+" "+
                        "\"json\": { },\r\n"+" "+
                        "\"origin\": "+Constants.ORIGIN+",\r\n"+" "+
                        "\"url\": "+this.uri+",\r\n"+
                        "}";
    }

    private String getFiles() {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < filesList.size(); i++) {
            temp.append(filesList.get(i) + ",");
        }
        return temp.toString();
    }

    private void getCurlOption(PrintWriter writer) {
        this.curlRequest = this.curlRequest.replace("get?","");
        if (this.curlRequest.matches(Constants.REG2)) {
            String[] temp = this.curlRequest.split("&");
            for (int i = 0; i < temp.length; i++) {
                setParameters(temp[i]);
            }
        } else {
            setParameters(this.curlRequest);
        }
        printOutput(getBody(), writer);
    }

    private String getBody() {
        return
                "{\r\n"+
                        "\"args\":{"+
                        getParameters()+"},\r\n"+
                        "\"headers\":{\r\n"+
                        getHeaders()+"},\r\n"+
                        "\"origin\": "+Constants.ORIGIN+",\r\n"+
                        "\"url\": "+this.uri+",\r\n"+
                        "}";
    }

    private String getParameters() {
        StringBuilder temp = new StringBuilder();
        temp.append("\r\n");
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            temp.append(" \"" + entry.getKey() + "\": \"" + entry.getValue() + "\",\r\n");
        }
        return temp.toString();
    }

    private void setParameters(String values) {
        String[] value = values.trim().split("=");
        parameters.put(value[0].trim(), value[1].trim());
    }

    private String getHeaders() {
        StringBuilder head = new StringBuilder();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            head.append(" " + entry.getKey() + ": " + entry.getValue() + "\r\n");
        }
        return head.toString();
    }

    private String getConnectionState() {
        String sCode = "";
        switch (this.statusCode) {
            case Constants.HTTP_200:
                sCode = "OK";
                break;
            case Constants.HTTP_400:
                sCode = "Bad Request";
                break;
            case Constants.HTTP_404:
                sCode = "Not Found";
                break;
            default:
                sCode = "ERROR HTTP";
                break;
        }
        return sCode;
    }

    private void addToHeaders(String inputRequest) {
        String [] values = inputRequest.split(":");
        this.headers.put(values[0], values[1]);
    }
}
