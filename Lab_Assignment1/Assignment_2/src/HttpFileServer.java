import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * HttpFileServer is the server system which for the file transfer
 *
 * @author  Naga Satish Reddy Dwarampudi
 * @author Namita Faujdar
 */
public class HttpFileServer {
    private int port;
    private String directoryPath;
    private boolean isDebugging;

    HttpFileServer(){
        this.port = 8080;
        this.directoryPath = System.getProperty("user.dir");
    }
    private void start() throws IOException {
        this.readStartCommand();
        try {
            ServerSocket serverSocket = new ServerSocket(this.port);
            System.out.println("Server is listening at" + this.port + " port with directory"+this.directoryPath+" with debugging "+this.isDebugging);
            if (this.isDebugging) {
                System.out.println("Server is listening at" + this.port + " port with directory"+this.directoryPath);
            }
            int clientCounter = 1;
            while (true) {
                Socket clientSocket = serverSocket.accept();
                if(this.isDebugging){
                    System.out.println("Connection established with client "+clientCounter);
                }
                HttpServerThread  serverThread = new HttpServerThread(clientSocket, this.directoryPath, clientCounter);
                new Thread(serverThread).start();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void readStartCommand() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String [] commandArray = reader.readLine().trim().split(" ");
        for(int i = 0; i < commandArray.length; i++) {
            switch(commandArray[i]){
                case Constants.PORT_CODE:
                    this.setHasPort(true);
                    this.setPort(Integer.parseInt(commandArray[++i]));
                    break;
                case Constants.PATH_DIRECTORY:
                    this.setPathDirectory(commandArray[++i]);
                    break;
                case Constants.DEBUGGING:
                    this.setDebugging(true);
                    break;
            }
        }
    }

    private void setDebugging(boolean b) {
        this.isDebugging = b;
    }

    private void setPathDirectory(String dirPath) {
        this.directoryPath = dirPath;
    }

    private void setPort(int portNumber) {
        this.port = portNumber;
    }

    private void setHasPort(boolean b) {
    }

    public static void main(String[] args) {
        HttpFileServer server = new HttpFileServer();
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
