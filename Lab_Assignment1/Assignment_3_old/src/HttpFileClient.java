import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class HttpFileClient {
    private boolean headerFlag;
    private List<String> headersList;
    private String url;
    private boolean bodyFlag;
    private String body;
    private String query;

    HttpFileClient(){
        this.headerFlag = false;
        this.bodyFlag = false;
        this.headersList = new ArrayList<>();
    }
    public static void main(String[] args) {
        HttpFileClient client = new HttpFileClient();
        try {
                client.startClient();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void startClient() throws IOException, URISyntaxException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        String[] inputArray = input.trim().split(" ");
        if(inputArray[0].equals("httpfs")) {
            for (int i = 0; i < inputArray.length; i++) {
                if (inputArray[i].equals(Constants.HEADER_CODE)) {
                    this.headerFlag = true;
                    this.headersList.add(inputArray[++i]);
                }
                if (inputArray[i].startsWith(Constants.HTTP) || inputArray[i].startsWith(Constants.HTTPS)) {
                    this.url = inputArray[i];
                }
                if (inputArray[i].startsWith(Constants.INLINE_DATA_CODE1) || inputArray[i].startsWith(Constants.INLINE_DATA_CODE2)) {
                    this.bodyFlag = true;
                    String temp;
                    if(input.contains(Constants.INLINE_DATA_CODE2)) {
                        temp = input.substring(input.indexOf(Constants.INLINE_DATA_CODE2) + 2).trim();
                    }else{
                        temp = input.substring(input.indexOf(Constants.INLINE_DATA_CODE1) + 1).trim();
                    }
                    this.body = temp;
                }
            }
            URI uri = new URI(this.url);
            Socket clientSocket = new Socket(uri.getHost(), uri.getPort());
            this.query = uri.getPath().substring(1).trim();
            System.out.println("Server connection establised for "+this.query);
            this.sendRequest(clientSocket);
        }else{
            System.out.println("Invalid Command!");
        }
    }

    private void sendRequest(Socket clientSocket) throws IOException {
        PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
        writer.println(this.query);
        if(this.headerFlag){
            for (String s : headersList) {
                writer.println(s);
            }
        }
        if(this.bodyFlag){
            writer.println("-d"+this.body);
        }
        writer.println("\r\n");
        writer.flush();
        this.receiveContent(clientSocket);
        writer.close();
        clientSocket.close();
    }

    private void receiveContent(Socket clientSocket) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String out;
        boolean filesFlag = false;
        List<String> fileList = new ArrayList<>();
        List<String> directoryList = new ArrayList<>();
        while((out = reader.readLine()) != null) {
            if(!this.query.equals(Constants.GETMETHOD+"/")){
                System.out.println(out);
            }else {
                filesFlag = true;
                String[] temp = out.split(">>");
                if(temp[0].trim().equals("Directory")){
                    directoryList.add(temp[1].trim());
                }else if(temp[0].trim().equals("File")){
                    fileList.add(temp[1].trim());
                }
            }
        }
        if (filesFlag) {
            System.out.println("------------");
            System.out.println("DIRECTORIES: ");
            System.out.println("------------");
            for (String str : directoryList) {
                System.out.println(str);
            }

            System.out.println("-------");
            System.out.println("FILES: ");
            System.out.println("-------");
            for (String str : fileList) {
                System.out.println(str);
            }
        }
        reader.close();
    }
}
