import java.net.InetAddress;

public class PacketBuilder {
    private int type;
    private long sequenceNumber;
    private InetAddress peerAddress;
    private int portNumber;
    private byte[] payload;

    public PacketBuilder setType(int typeOfPacket) {
        this.type = typeOfPacket;
        return this;
    }

    public PacketBuilder setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
        return this;
    }

    public PacketBuilder setPeerAddress(InetAddress peerAddress) {
        this.peerAddress = peerAddress;
        return this;
    }

    public PacketBuilder setPortNumber(int portNumber) {
        this.portNumber = portNumber;
        return this;
    }

    public PacketBuilder setPayload(byte[] payload) {
        this.payload = payload;
        return this;
    }

    public Packet build() {
        return new Packet(this.type, this.sequenceNumber, this.peerAddress, this.portNumber, this.payload);
    }
}
