import java.io.*;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

public class HttpServerThread implements Runnable {

    private final SocketAddress router;
    private final DatagramChannel channel;
    private final Packet packet;
    private final String directoryPath;
    private final ByteBuffer byteBuffer;
    private final ArrayList<String> fileHeader;
    private final ArrayList<String> filesList;
    private final HashMap<String, String> parameters;
    private final HashMap<String, String> headers;
    private boolean httpcFlag;
    private String clientRequest;
    private boolean httpfsFlag;
    private boolean contentTypeFlag;
    private boolean dispositionFlag;
    private String body;
    private final StringBuilder httpfsResponse = new StringBuilder();
    private String statusCode;
    private String uri;
    private String content;
    private boolean attachmentFlag;
    private boolean fileFlag;
    private boolean inLineFlag;

    public HttpServerThread(DatagramChannel channel, Packet packet, int counter, String directoryPath, ByteBuffer byteBuffer, SocketAddress router) {
        this.channel = channel;
        this.packet = packet;
        this.directoryPath = directoryPath;
        this.byteBuffer = byteBuffer;
        this.router = router;
        fileHeader = new ArrayList<>();
        filesList = new ArrayList<>();
        parameters = new HashMap<>();
        headers = new HashMap<>();
        headers.put("Connection", "keep-alive");
        headers.put("Host", "Localhost");
        headers.put("Date", Instant.now().toString());
    }

    @Override
    public void run() {
        int count = 0;
        try {
            String payload = new String(packet.getPayload(), UTF_8);
            System.out.println("payload==" + payload);
            BufferedReader in = new BufferedReader(new StringReader(payload));

            String inputRequest;
            while ((inputRequest = in.readLine()) != null) {
                if (inputRequest.endsWith(Constants.HTTP_VERSION)) {
                    this.httpcFlag = true;
                    this.clientRequest = inputRequest;
                } else if (inputRequest.matches("(GET|POST)/(.*)")) {
                    this.httpfsFlag = true;
                    this.clientRequest = inputRequest;
                }
                if (this.httpfsFlag) {
                    if (inputRequest.isEmpty())
                        break;
                    fileHeader.add(inputRequest);
                    contentTypeFlag = inputRequest.startsWith(Constants.CONTENT_TYPE) || this.contentTypeFlag;
                    dispositionFlag = inputRequest.startsWith(Constants.CONTENT_DISPOSITION) || this.dispositionFlag;
                    System.out.println("Input Request " + inputRequest);
                    System.out.println("CONTENT_TYPE " + inputRequest.startsWith(Constants.CONTENT_TYPE));

                    System.out.println("disposition " + inputRequest.startsWith(Constants.CONTENT_DISPOSITION));
                    this.body = inputRequest.startsWith(Constants.INLINE_DATA_CODE2) ? inputRequest.substring(2)
                            : this.body;
                }
                if (this.httpcFlag) {
                    if (inputRequest.matches(Constants.REG1)) {
                        if (count == 0) {
                            this.addHeaders(inputRequest);
                        }
                    }
                    if (count == 1) {
                        this.content = inputRequest;
                        break;
                    }
                    if (inputRequest.isEmpty())
                        count++;
                }
            }
            processClientRequest();
            this.httpfsResponse.append("\n");

            //create packet of Response
            Packet p = packet.toBuilder()
                    .setPayload(this.httpfsResponse.toString().getBytes())
                    .build();
            channel.send(p.toBuffer(), this.router);
            System.out.println("Sending " + this.httpfsResponse.toString() + " to router at " + 8080);
            System.out.println("Sending empty message to router at " + 3000);
//            logger.info("Sending \"{}\" to router at {}", this.httpFsRespinse.toString() + 8080);
//            logger.info("Sending \"{}\" to router at {}", "empty message", 3000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processClientRequest() {
        if (this.httpcFlag && this.clientRequest.matches("(GET|POST) /(.*)")) {
            createCurlRequest();
        }
        if (this.httpfsFlag) {
            System.out.println("Client Request : " + this.clientRequest);
            if (this.clientRequest.startsWith(Constants.GET_METHOD)) {
                processGetRequest();
            } else if (this.clientRequest.startsWith(Constants.POST_METHOD)) {
                processPostRequest();
            }
        }
    }

    private void processPostRequest() {

        if (!this.clientRequest.substring(5).contains(Constants.SLASH)) {
            try {
                File filePath = this.contentTypeFlag
                        ? new File(createFilePath(this.clientRequest.substring(5))
                        + getFileContentHeader())
                        : new File(createFilePath(this.clientRequest.substring(5)));

                PrintWriter printWriter = new PrintWriter(filePath);
                printWriter.println(this.body);
                displayOutput("Operation Status : Succcess");
                printWriter.close();
            } catch (FileNotFoundException e) {
                displayOutput(Constants.HTTP_404_ERROR);
            }
        } else {
            displayOutput("Error: " + Constants.ACCESS_DENIED);
        }

    }

    private void displayOutput(String message) {
        this.httpfsResponse.append(message);
        System.out.println(message);
    }

    private String getFileContentHeader() {
        String ext = "";
        for (String s : fileHeader) {
            if (s.trim().startsWith(Constants.CONTENT_TYPE)) {
                String[] temp = s.trim().split(":");
                ext = getExtension(temp[1].trim());
            }
        }
        return ext.trim();
    }

    private String getExtension(String extension) {

        String temp;
        switch (extension.trim()) {
            case "application/text":
                temp = ".txt";
                break;
            case "application/json":
                temp = ".json";
                break;
            default:
                temp = "";
                break;
        }
        return temp;

    }

    private void processGetRequest() {
        String fileName = this.contentTypeFlag ? this.clientRequest.substring(4) + getFileContentHeader()
                : this.clientRequest.substring(4);
        File filePath = new File(createFilePath(fileName));
        if (createFilePath(fileName).contains("/..")) {
            displayOutput("Error: " + Constants.ACCESS_DENIED);
        } else if (this.contentTypeFlag) {
            try {
                if (filePath.exists()) {
                    BufferedReader br = new BufferedReader(new FileReader(filePath));
                    String line;
                    this.httpfsResponse.append("File Content");
                    while ((line = br.readLine()) != null) {
                        this.httpfsResponse.append(line);
                    }
                    displayOutput("Operation Status : Success");
                    br.close();
                } else {
                    displayOutput(Constants.HTTP_404_ERROR);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (!fileName.contains(Constants.SLASH)) {
            if (filePath.exists()) {
                if (filePath.isDirectory()) {
                    HashMap<String, ArrayList<String>> output = new HashMap<>();
                    output.put(Constants.DIRECTORY, new ArrayList<>());
                    output.put(Constants.FILE, new ArrayList<>());
                    for (File file : Objects.requireNonNull(filePath.listFiles())) {
                        if (file.isDirectory()) {
                            ArrayList<String> temp = output.get(Constants.DIRECTORY);
                            temp.add(file.getName());
                            output.replace(Constants.DIRECTORY, temp);
                        } else if (file.isFile()) {
                            ArrayList<String> temp = output.get(Constants.FILE);
                            temp.add(file.getName());
                            output.replace(Constants.FILE, temp);
                        }
                    }
                    System.out.println("------------\nDIRECTORIES: \n------------");
                    for (Map.Entry<String, ArrayList<String>> entry : output.entrySet()) {
                        ArrayList<String> temp = entry.getValue();
                        for (String s : temp) {
                            if (entry.getKey().equals(Constants.DIRECTORY)) {
                                displayOutput(entry.getKey().trim() + s.trim());
                            }
                        }
                    }
                    System.out.println("------------\n FILES : \n------------");
                    for (Map.Entry<String, ArrayList<String>> entry : output.entrySet()) {
                        ArrayList<String> temp = entry.getValue();
                        for (String s : temp) {
                            if (entry.getKey().equals(Constants.FILE)) {
                                displayOutput(entry.getKey().trim() + s.trim());
                            }
                        }
                    }
                } else if (filePath.isFile()) {
                    PrintWriter fileWriter = null;
                    File downloadPath = new File(this.directoryPath + Constants.PATH_TO_DOWNLOAD);
                    if (this.dispositionFlag) {
                        getFileDispositionHeader();
                        if ((this.attachmentFlag && !downloadPath.exists())) {
                            System.out.println("Download Folder Created : "
                                    + new File(this.directoryPath + Constants.PATH_TO_DOWNLOAD).mkdir());
                        }
                    }
                    try {
                        if (this.attachmentFlag) {
                            fileWriter = this.fileFlag
                                    ? new PrintWriter(downloadPath.getPath() + Constants.SLASH + getFileDispositionHeader())
                                    : new PrintWriter(downloadPath.getPath() + Constants.SLASH + fileName);
                        }
                        BufferedReader br = new BufferedReader(new FileReader(filePath));
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (this.dispositionFlag) {
                                if (this.inLineFlag) {
                                    displayOutput(line + "\n");
                                } else if (this.attachmentFlag) {
                                    Objects.requireNonNull(fileWriter).println(line);
                                }
                            } else {
                                displayOutput(line + "\n");
                            }

                        }
                        displayOutput("Operation Status : Success");
                        if (this.attachmentFlag) {
                            Objects.requireNonNull(fileWriter).close();
                        }
                        br.close();
                    } catch (FileNotFoundException e) {
                        displayOutput(Constants.HTTP_404_ERROR + Constants.FILE_NOT_FOUND);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else {
                displayOutput(Constants.HTTP_404_ERROR);
            }
        } else {
            displayOutput("Error: " + Constants.ACCESS_DENIED);
        }
    }

    private String getFileDispositionHeader() {

        String name = "";
        for (String s : fileHeader) {
            if (s.trim().startsWith(Constants.CONTENT_DISPOSITION)) {
                String[] temp = s.trim().split(";");
                String[] temp2 = temp[0].trim().split(":");
                if (temp2[1].trim().equals("attachment")) {
                    this.attachmentFlag = true;
                    if (temp.length == 2) {
                        String[] temp3 = temp[1].trim().split(":");
                        name = temp3[1].trim();
                        this.fileFlag = true;
                    }
                }
                if (temp2[1].trim().equals("inline")) {
                    this.inLineFlag = true;
                }
            }
        }
        return name.trim();

    }

    private String createFilePath(String fileName) {
        return this.directoryPath + Constants.SLASH + fileName;
    }

    private void createCurlRequest() {
        this.clientRequest = this.clientRequest.replace("GET /", "");
        this.clientRequest = this.clientRequest.replace("POST /", "");
        this.clientRequest = this.clientRequest.replace("HTTP/1.1", "");
        this.statusCode = Constants.HTTP_200;
        this.uri = "http://" + Constants.IP_ADDRESS + ":" + this.packet.getPortNumber() + Constants.SLASH + this.clientRequest;
        this.httpfsResponse.append("HTTP/1.0 ").append(this.statusCode).append(" ").append(getConnectionState()).append("\r\n").append(getHeaders()).append("\n");
        checkCurlOption();
    }

    private void checkCurlOption() {
        if (this.clientRequest.startsWith("get?")) {
            System.out.println("CURL : Request Type GET");
            getCurlOption();
        } else if (this.clientRequest.startsWith("post?")) {
            System.out.println("CURL : Request Type POST");
            postCurlOption();
        }
    }

    private void postCurlOption() {
        this.clientRequest = this.clientRequest.replace("post?", "");
        if (!this.clientRequest.isEmpty() && this.clientRequest.matches("(.*)=(.*)")) {
            if (this.clientRequest.matches(Constants.REG2)) {
                String[] temp = this.clientRequest.split("&");
                for (String s : temp) {
                    setParameters(s);
                }
            } else {
                setParameters(this.clientRequest);
            }
        }
        this.httpfsResponse.append(getPostBody()).append("\n");

    }

    private String getPostBody() {

        return
                "{\r\n" + " " +
                        "\"args\":{" + " " +
                        getParameters() + "},\r\n" + " " +
                        "\"data\":{" + " " +
                        this.content + "},\r\n" + " " +
                        "\"files\":{\r\n" + " " +
                        getFiles() + "},\r\n" + " " +
                        "\"headers\":{\r\n" +
                        getHeaders() + " },\r\n" + " " +
                        "\"json\": { },\r\n" + " " +
                        "\"origin\": " + Constants.ORIGIN + ",\r\n" + " " +
                        "\"url\": " + this.uri + ",\r\n" +
                        "}";

    }

    private String getFiles() {

        StringBuilder temp = new StringBuilder();
        for (String s : filesList) {
            temp.append(s).append(",");
        }
        return temp.toString();

    }

    private String getParameters() {

        StringBuilder temp = new StringBuilder();
        temp.append("\r\n");
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            temp.append(" \"").append(entry.getKey()).append("\": \"").append(entry.getValue()).append("\",\r\n");
        }
        return temp.toString();

    }

    private void setParameters(String parameter) {
        String[] value = parameter.trim().split("=");
        parameters.put(value[0].trim(), value[1].trim());
    }

    private void getCurlOption() {
        this.clientRequest = this.clientRequest.replace("get?", "");
        if (this.clientRequest.matches(Constants.REG2)) {
            String[] temp = this.clientRequest.split("&");
            for (String s : temp) {
                setParameters(s);
            }
        } else {
            setParameters(this.clientRequest);
        }
        this.httpfsResponse.append(getGetBody()).append("\n");

    }

    private String getGetBody() {

        return
                "{\r\n" +
                        "\"args\":{" +
                        getParameters() + "},\r\n" +
                        "\"headers\":{\r\n" +
                        getHeaders() + "},\r\n" +
                        "\"origin\": " + Constants.ORIGIN + ",\r\n" +
                        "\"url\": " + this.uri + ",\r\n" +
                        "}";

    }

    private String getHeaders() {
        StringBuilder head = new StringBuilder();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            head.append(" ").append(entry.getKey()).append(": ").append(entry.getValue()).append("\r\n");
        }
        return head.toString();
    }

    private String getConnectionState() {
        String sCode;
        switch (this.statusCode) {
            case Constants.HTTP_200:
                sCode = "OK";
                break;
            case Constants.HTTP_400:
                sCode = "Bad Request";
                break;
            case Constants.HTTP_404:
                sCode = "Not Found";
                break;
            default:
                sCode = "ERROR HTTP";
                break;
        }
        return sCode;
    }

    private void addHeaders(String headerValues) {
        String[] value = headerValues.trim().split(":");
        headers.put(value[0].trim(), value[1].trim());
    }
}