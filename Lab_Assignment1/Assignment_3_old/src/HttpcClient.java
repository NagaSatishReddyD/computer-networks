import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HttpcClient {
    private final String userInput;
    private boolean verboseFlag;
    private boolean headerFlag;
    private final List<String> headerList = new ArrayList<>();
    private boolean inLineDataFlag;
    private String inLineData;
    private boolean readFileFlag;
    private String readFile;
    private boolean generateFileFlag;
    private String generateFile;
    private String url;
    private UrlData urlData;
    private Socket socket;
    private StringBuilder sb;
    private int statusCode;
    private String newUrl;

    public HttpcClient(String input) {
        this.userInput = input;
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        try{
            while (!(input = reader.readLine()).equals("exit")) {
                HttpcClient library = new HttpcClient(input);
                library.parseInput();
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void parseInput() throws IOException {
        String[] splitInput = this.userInput.split(" ");
        List<String> inputList = Arrays.stream(splitInput).filter(x -> x.trim().length() != 0).collect(Collectors.toList());
        if (inputList.get(0).equals(Constants.HTTPC) && inputList.get(1).equals(Constants.HELP)) {
            printHelpContent(inputList.size() == 2 ? "NOTHING" : inputList.get(2));
        } else if (inputList.get(0).equals(Constants.HTTPC) && (inputList.get(1).equals(Constants.GET) || inputList.get(1).equals(Constants.POST))) {
            for (int i = 2; i < inputList.size(); i++) {
                switch (inputList.get(i)) {
                    case Constants.VERBOSE:
                        this.verboseFlag = true;
                        break;
                    case Constants.HEADER:
                        this.headerFlag = true;
                        headerList.add(inputList.get(++i));
                        break;
                    case Constants.INLINE_DATA_CODE1:
                    case Constants.INLINE_DATA_CODE2:
                        this.inLineDataFlag = true;
                        for (int j = ++i; j < inputList.size(); j++) {
                            if (inputList.get(j).endsWith("}")) {
                                this.inLineData += inputList.get(j);
                                break;
                            } else {
                                this.inLineData += inputList.get(j);
                            }
                        }
                        break;
                    case Constants.READ_FILE_CODE:
                        this.readFileFlag = true;
                        this.readFile = inputList.get(++i);
                        break;
                    case Constants.CREATE_FILE_CODE:
                        this.generateFileFlag = true;
                        this.generateFile = inputList.get(++i);
                        break;
                    default:
                        break;
                }
                if (inputList.get(i).startsWith("http://") || inputList.get(i).startsWith("https://")) {
                    this.url = inputList.get(i);
                }
            }
            processRequest(inputList.get(1).trim());
        } else {
            System.out.println("Invalid Command");
        }
    }

    private void processRequest(String requestType) throws IOException {
        if (this.url != null) {
            this.urlData = new UrlData(this.url);
            if (!(this.readFileFlag && this.inLineDataFlag)) {
                if (requestType.equals(Constants.POST)) {
                    postRequest();
                } else if (requestType.equals(Constants.GET)) {
                    getRequest();
                } else {
                    System.out.println("Get or Post Request Not Found...");
                }
            } else {
                System.out.println("Invalid Command. Both -f and -d cannot be together");
            }
        } else {
            System.out.println("Invalid URL");
        }
    }

    private void getRequest() throws IOException {
        sb = new StringBuilder();
        if (!(this.readFileFlag || this.inLineDataFlag)) {
            this.socket = new Socket(this.urlData.getHostName(), this.urlData.getPortNumber());
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            if (this.urlData.getUrlPath().length() == 0) {
                sb.append("GET / HTTP/1.1\n");
            } else {
                sb.append("GET ").append(this.urlData.getUrlPath()).append(" HTTP/1.1\n");
            }
            sb.append("Host:").append(this.urlData.getHostName()).append("\n");
            if (!headerList.isEmpty()) {

                for (String headerData : this.headerList) {
                    if (this.headerFlag) {
                        String[] headerKeyValue = headerData.split(":");
                        sb.append(headerKeyValue[0]).append(":").append(headerKeyValue[1]).append("\n");
                    }
                }
                sb.append("\r\n\n");
                writer.flush();
                displayOutput();
                writer.close();
                isRedirect(Constants.GET_REDIRECT);
            } else {
                System.out.println("Invalid Command : In GET Request -f or -d are not allowed ");
            }
        }
    }

    private void isRedirect(String requestRedirect) throws IOException {
        if(this.statusCode != Constants.HTTP_OK && (this.statusCode == Constants.HTTP_MOVED_TEMP || this.statusCode == Constants.HTTP_MOVED_PERM || this.statusCode == Constants.HTTP_SEE_OTHER)){
            this.readFileFlag = true;
            sleepThread(1000);
            System.out.println("\nStatus Code :" + this.statusCode);
            sleepThread(1000);
            System.out.print("Redirecting to:" + this.newUrl + "\n Please Wait............");
            sleepThread(2000);
            this.url = this.newUrl;
            this.urlData = new UrlData(this.url);
            if (requestRedirect.equals(Constants.GET_REDIRECT))
                getRequest();
            else if (requestRedirect.equals(Constants.POST_REDIRECT))
                postRequest();
            System.out.println("Validated : Redirection");
        }
    }
    public static void sleepThread(int seconds) {
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private void displayOutput() throws IOException {
        InputStream inputStream = this.socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String output;
        boolean entryFlag = false;
        boolean divideFlag = true;
        StringBuilder receiveContent = new StringBuilder();
        do {
            output = reader.readLine();
            if (output.trim().isEmpty()) {
                entryFlag = true;
                if (entryFlag && divideFlag) {
                    divideFlag = false;
                    receiveContent.append("Content Separated");
                }
            }
            receiveContent.append(output);
            receiveContent.append("Entry Separated");
        } while ((output.trim() != null) && !(output.endsWith("</html>") || output.endsWith("}")
                || output.endsWith("post") || output.endsWith("/get")));

        reader.close();
        String[] splitReceiveContent = receiveContent.toString().split("Content Separated");
        String[] responseHeader = splitReceiveContent[0].split("Entry Separated");
        String[] responseBody = splitReceiveContent[1].split("Entry Separated");
        this.statusCode = Integer.parseInt(responseHeader[0].substring(9, 12));
        for(String header: responseHeader){
            if(header.startsWith("Location:")){
                this.newUrl = header.substring(10);
            }
        }
        printVerboseData(responseHeader);
        printOutput(responseBody);
        isGenerateFile(responseHeader, responseBody);
    }

    private void isGenerateFile( String[] headers, String[] messageBody) {
        if (this.generateFileFlag) {
            PrintWriter writer;
            if (this.generateFile != null) {
                try {
                    writer = new PrintWriter(generateFile, "UTF-8");
                    writer.println("Command: " + this.userInput + "\r\n");
                    if (this.verboseFlag)
                        printOutputInFile(writer, headers);

                    writer.println("");
                    printOutputInFile(writer, messageBody);
                    writer.close();
                } catch (FileNotFoundException | UnsupportedEncodingException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    private void printOutputInFile(PrintWriter writer, String[] messageData) {
        for(String message: messageData){
            writer.println(message);
        }
    }

    private void printOutput(String[] responseBody) {
        for(String messageData : responseBody) {
            System.out.println(messageData);
        }
        System.out.println();
    }

    private void printVerboseData(String[] responseHeader) {
        if(this.verboseFlag){
            printOutput(responseHeader);
        }
    }

    private void postRequest() throws IOException {
        this.socket = new Socket(this.urlData.getHostName(), this.urlData.getPortNumber());
        StringBuilder sb = new StringBuilder();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        if (this.urlData.getUrlPath().length() != 0) {
            writer.write(generateMethodURL("POST", this.urlData.getUrlPath(), " HTTP/1.1\r\n"));
        } else {
            writer.write(generateMethodURL("POST", "", " HTTP/1.1\r\n"));
        }
        writer.write("Host:" + this.urlData.getHostName() + "\r\n");
        if (this.headerFlag && !headerList.isEmpty()) {
            for(String header : headerList) {
                String[] headerKeyValue = header.split(":");
                writer.write(headerKeyValue[0] + ":" + headerKeyValue[1] + "\r\n");
            }
        }
        if (this.inLineDataFlag) {
            writer.write("Content-Length:" + inLineData.length() + "\r\n");
        } else if (this.readFileFlag) {
            BufferedReader reader = new BufferedReader(new FileReader(readFile));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            writer.write("Content-Length:" + sb.toString().length() + "\r\n");
            System.out.println("Data " + sb.toString());
            reader.close();
        }
        writer.write("\r\n");
        if (inLineData != null) {
            inLineData = inLineData.replace("\'", "");
            writer.write(inLineData);
            writer.write("\r\n");
        }
        if (sb.toString() != null) {
            writer.write(sb.toString());
            writer.write("\r\n");
        }
        writer.flush();
        displayOutput();
        writer.close();
        isRedirect(Constants.POST_REDIRECT);
    }

    private String generateMethodURL(String method, String tempUrl, String type) {
        if (method.equals("POST")) {
            if (tempUrl.length() != 0) {
                return "POST " + tempUrl + " HTTP/1.1\r\n";
            } else {
                return "POST / HTTP/1.1\r\n";
            }
        } else if (method.equals("GET")) {
            if (tempUrl.length() == 0) {
                return "GET / /1.1";
            } else {
                return "GET " + tempUrl + " HTTP/1.1";
            }
        }
        return "";
    }

    private void printHelpContent(String helpOption) {
        switch (helpOption) {
            case Constants.POST:
                System.out.println(
                        "usage: httpc post [-v] [-h key:value] [-d inline-data] [-f file] URL\nPost executes a HTTP POST request for a given URL with inline data or from file.\n -v Prints the detail of the response such as protocol, status, and headers.\n -h key:value Associates headers to HTTP Request with the format 'key:value'.\n -d string Associates an inline data to the body HTTP POST request. \n -f file Associates the content of a file to the body HTTP POST request.\nEither [-d] or [-f] can be used but not both.");

                break;
            case Constants.GET:
                System.out.println("usage: httpc get [-v] [-h key:value] URL\r\n"
                        + "Get executes a HTTP GET request for a given URL.\r\n"
                        + " -v Prints the detail of the response such as protocol, status,\r\n" + "and headers.\r\n"
                        + " -h key:value Associates headers to HTTP Request with the format\r\n" + "'key:value'.");
                break;
            case "NOTHING":
                System.out.println(
                        "httpc is a curl-like application but supports HTTP protocol only.\r\n"
                                + "Usage:\r\n\t httpc command [arguments] \r\nThe commands are: \r\n\tget\texecutes a HTTP GET request and prints the response.\r\n\tpost\texecutes a HTTP POST request and prints the response. \r\n\thelp\tprints this screen. \r\nUse \"httpc help [command]\" for more information about a command.");
                break;
        }
    }
}
