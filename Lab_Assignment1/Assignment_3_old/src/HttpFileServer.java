import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * HttpFileServer is the server system which for the file transfer
 *
 * @author  Naga Satish Reddy Dwarampudi
 * @author Namita Faujdar
 */
public class HttpFileServer {
    private int port;
    private String directoryPath;
    private boolean isDebugging;
    private DatagramChannel channel;
    private ByteBuffer byteBuffer;

    HttpFileServer(){
        this.port = 8080;
        this.directoryPath = System.getProperty("user.dir");
    }
    private void readInput() throws IOException {
        this.readStartCommand();
        DatagramChannel channel = DatagramChannel.open();
        System.out.println("Server Started");
        if(this.isDebugging){
            System.out.println("EchoServer is listenign at "+channel.getLocalAddress());
        }

        ByteBuffer buffer = ByteBuffer.allocate(Constants.MAX_LEN).order(ByteOrder.BIG_ENDIAN);
        this.channel = channel;
        this.byteBuffer = buffer;
    }

    private void readStartCommand() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String [] commandArray = reader.readLine().trim().split(" ");
        for(int i = 0; i < commandArray.length; i++) {
            switch(commandArray[i]){
                case Constants.PORT_CODE:
                    this.setHasPort(true);
                    this.setPort(Integer.parseInt(commandArray[++i]));
                    break;
                case Constants.PATH_DIRECTORY:
                    this.setPathDirectory(commandArray[++i]);
                    break;
                case Constants.DEBUGGING:
                    this.setDebugging(true);
                    break;
            }
        }
    }

    private void setDebugging(boolean b) {
        this.isDebugging = b;
    }

    private void setPathDirectory(String dirPath) {
        this.directoryPath = dirPath;
    }

    private void setPort(int portNumber) {
        this.port = portNumber;
    }

    private void setHasPort(boolean b) {
    }

    public static void main(String[] args) {
        HttpFileServer server = new HttpFileServer();
        try {
            server.readInput();
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void start() throws IOException {
        int counter = 0;
        while (true) {
            this.byteBuffer.clear();
            SocketAddress router = this.channel.receive(this.byteBuffer);
            this.byteBuffer.flip();
            Packet packet = Packet.fromBuffer(this.byteBuffer);
            this.byteBuffer.flip();
            String payload = new String(packet.getPayload(), UTF_8);
            System.out.println("Packet: "+ packet);
            System.out.println("Payload: "+payload);
            System.out.println("Router:"+router);
            validateRequest(payload, router, counter, packet);
        }
    }

    private void validateRequest(String payload, SocketAddress router, int counter, Packet packet) throws IOException {
        long packetNumber = packet.getSequenceNumber();
        String ACK = "send packet from "+(++packetNumber);
        if (packet.getType() == Constants.CONNECTION_TYPE && !payload.equals(Constants.ACK_CODE)) {
            Packet resp = packet.toBuilder().setPayload(ACK.getBytes()).build();
            channel.send(resp.toBuffer(), router);
            System.out.println(">> Client with " + packet.getPortNumber() + " connection established");
            System.out.println(packet.getType());
            System.out.println("Payload :" +payload);

        } else if (packet.getType() == Constants.DATA_TYPE && !payload.equals(Constants.ACK_CODE)) {

            HttpServerThread serverThread = new HttpServerThread(channel, packet, counter, directoryPath, this.byteBuffer, router);
            Thread t = new Thread(serverThread);
            t.start();

        } else if (packet.getType() == Constants.DATA_TYPE && payload.equals(Constants.ACK_CODE)) {
            System.out.println("DONE");
        }
        this.byteBuffer.clear();
    }
}
