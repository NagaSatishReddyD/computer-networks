import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Packet {
    private int type;
    private long sequenceNumber;
    private InetAddress peerAddress;
    private int portNumber;
    private byte[] payload;

    public Packet(int type, long sequenceNumber, InetAddress peerAddress, int portNumber, byte[] payload) {
        this.type = type;
        this.sequenceNumber = sequenceNumber;
        this.peerAddress = peerAddress;
        this.portNumber = portNumber;
        this.payload = payload;
    }

    public static Packet fromBuffer(ByteBuffer byteBuffer) throws IOException {
        if(byteBuffer.limit() < Constants.MIN_LEN || byteBuffer.limit() > Constants.MAX_LEN){
            System.out.println(byteBuffer.limit());
            throw new IOException("Invalid Exception");
        }
        PacketBuilder builder = new PacketBuilder();
        byte[] host = new byte[]{byteBuffer.get(), byteBuffer.get(), byteBuffer.get(), byteBuffer.get()};
        byte[] payload = new byte[byteBuffer.remaining()];
        byteBuffer.get(payload);
        return builder
                .setType(Byte.toUnsignedInt(byteBuffer.get()))
                .setSequenceNumber(Integer.toUnsignedLong(byteBuffer.getInt()))
                .setPeerAddress(Inet4Address.getByAddress(host))
                .setPortNumber(Short.toUnsignedInt(byteBuffer.getShort()))
                .setPayload(payload).build();
    }
    
    public void setType(int typeOfPacket) {
        this.type = typeOfPacket;
    }

    public void setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setPeerAddress(InetAddress peerAddress) {
        this.peerAddress = peerAddress;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public void setPayload(byte[] payload) {
        this.payload = payload;
    }

    public byte[] getPayload() {
        return this.payload;
    }

    public long getSequenceNumber() {
        return this.sequenceNumber;
    }

    public int getType() {
        return this.type;
    }

    public int getPortNumber() {
        return this.portNumber;
    }

    public PacketBuilder toBuilder() {
        return new PacketBuilder()
                .setType(this.type)
                .setSequenceNumber(this.sequenceNumber)
                .setPeerAddress(this.peerAddress)
                .setPortNumber(this.portNumber)
                .setPayload(this.payload);
    }

    public ByteBuffer toBuffer() {
        ByteBuffer buf = ByteBuffer.allocate(Constants.MAX_LEN).order(ByteOrder.BIG_ENDIAN);
        write(buf);
        buf.flip();
        return buf;
    }

    private void write(ByteBuffer buf) {
        buf.put((byte) type);
        buf.putInt((int) sequenceNumber);
        buf.put(peerAddress.getAddress());
        buf.putShort((short) portNumber);
        buf.put(payload);
    }
}
